/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.control;

import java.util.ArrayList;
import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;
import occ.ues.edu.sv.ingenieria.prn335.control.Motor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author luismarioram99
 */
public class MotorTest {
    
    public MotorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of mostrarSucursales method, of class Motor.
     */
    @Test
    public void testMostrarSucursales() {
        
    }

    /**
     * Test of modificarSucursal method, of class Motor.
     */
    @Test
    public void testModificarSucursal() {
        
        Motor pruebaMotor = new Motor();        
        
        //datos de prueba
        int id_sucursal = 3;
        String nombre = "Cinepolis del centro";
        String ciudad = "Acajutla";
        String departamento = "colorado";
        String contacto = "Will Smith";
        boolean estado = true;
        boolean success;
        
        
        //ejecucion de la prueba
        success = pruebaMotor.modificarSucursal(id_sucursal, nombre, ciudad, departamento, contacto, estado);
        
        Sucursal resultado = pruebaMotor.mostrarSucursales().get(id_sucursal-1);
        //Revisar que los datos concuerden con los pasado, aka, que se hayan modificado
        assertSame(nombre,resultado.getNombre());
        assertSame(ciudad,resultado.getCiudad());
        assertSame(departamento,resultado.getDepartamento());
        assertSame(contacto,resultado.getContacto());
        assertSame(estado,resultado.isEstado());
        
        //revisar que retorna exito cuando debe
        assertTrue(success);
        
        //no debe funcionar pues ya esta activo ese objeto
        success = pruebaMotor.modificarSucursal(id_sucursal, nombre, ciudad, departamento, contacto, estado);
        
        assertFalse(success);
        
        //otro objeto inactivo
        id_sucursal = 4;
        estado = false;
        
        
        
        //se mandan los datos de prueba para estandarizar
        success = pruebaMotor.modificarSucursal(id_sucursal, nombre, ciudad, departamento, contacto, estado);     
        resultado = pruebaMotor.mostrarSucursales().get(id_sucursal - 1);
        
        //se mandan vacios para comprobar que cuando son vacios no se editan
        success = pruebaMotor.modificarSucursal(id_sucursal, "","", "", "", true);
        
        
        
        //se comprueba
        assertSame(nombre,resultado.getNombre());
        assertSame(ciudad,resultado.getCiudad());
        assertSame(departamento,resultado.getDepartamento());
        assertSame(contacto,resultado.getContacto());
        
        //se checkea si retorna true
        assertTrue(success);
       
    }

    /**
     * Test of buscarActivos method, of claszs Motor.
     */
    @Test
    public void testBuscarActivos() {
        
        
        
        //fail("The test case is a prototype.");
    }
    
}
