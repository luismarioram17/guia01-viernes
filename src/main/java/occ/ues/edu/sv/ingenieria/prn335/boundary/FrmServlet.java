/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.boundary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;
import occ.ues.edu.sv.ingenieria.prn335.control.Motor;

/**
 *
 * @author luismarioram99
 */
@WebServlet(name = "sucursalServlet", urlPatterns = {"/sucursalServlet"})
public class FrmServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Guia01-viernes servlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Resultado:</h1>");
            
            //cual boton fue presionado
            
            String filtrar = request.getParameter("filtrar");
            String editar = request.getParameter("editar");
            
            String accion;
            
            //decision de accion
            
            accion = (filtrar == null)?"editar":"filtrar";
            
            out.println("<h1>"+ accion +"</h1>");
            
            //decision de accion
            
            //instancia de clase
            Motor motorControl = new Motor();
            ArrayList<Sucursal> listaSucursales = null;
        
            if(accion.equals("editar")){
                
                //acciones para editar            
                    
                //obtener id;
                String idSucursal = request.getParameter("id-sucursal");
                //si la id no es valida, se queda en -1
                int idSucursalNum = -1;
                                
                //si no selecciona sucursal, no puede editar
                if(idSucursal.equals("")){
                    
                    out.println("<p style=\"color:red\">Seleccione una sucursal.</p>");
                    
                }else{
                    //de otro modo, adquiere la id
                    idSucursalNum = Integer.parseInt(idSucursal);
                    
                }
                
                
                //variables a modificar
                String nombre = request.getParameter("nombre");
                String ciudad = request.getParameter("ciudad");
                String departamento = request.getParameter("departamento");
                String contacto = request.getParameter("contacto");
                String estado = request.getParameter("estado");
                
                //si estado es invalido, estadoValido = false;                
                boolean estadoActivo = false;
                boolean estadoValido = true;
                
                boolean exitoEditar;
                //activo -> true
                //inactivo -> false
                if(estado.equals("activo")){
                    
                    estadoActivo = true;
                    
                }else if(estado.equals("inactivo")){
                    
                    estadoActivo = false;
                    
                }else{
                    
                    //para otra entrada
                    estadoValido = false;
                    
                }
                
                //informa al usuario
                if(!estadoValido){
                    
                    out.println("<p style=\"color:red\">Los estados validos son: activo/inactivo</p>");
                    
                }
                
                if(idSucursalNum != -1 && estadoValido){
                    
                    //edita los con los valores
                    exitoEditar = motorControl.modificarSucursal(idSucursalNum,nombre,ciudad,departamento,contacto,estadoActivo);
                    
                    if(exitoEditar){
                        //si hay exito
                        out.println("<p style=\"color:gren\">Exito al editar!</p>");
                        
                    }else{
                        //si hay fracaso, como mi vida
                        out.println("<p style=\"color:red\">Fallo al editar :(</p>");
                        
                    }
                    
                }
                
            }else{
                
                //acciones para filtrar                
                String departamento = request.getParameter("departamento");
                
                //si no seleccionan un departamento
                if(!departamento.equals("")){
                    
                    //filtra los datos a listaSucursales
                    listaSucursales = motorControl.buscarActivos(departamento);
                    
                }else{
                    
                    //de otra manera no se pueden filtrar
                    out.println("<p style=\"color:red\">Para filtrar: seleccione un departamento</p>");
                    
                }
                
            }
            
            //si no fueron filtrados o si se edito la tabla
            if(listaSucursales == null){
                
                listaSucursales = motorControl.mostrarSucursales();
                
            }
            
            //impresion de tabla
            out.println("<table border=\"1\">");
            out.println("<tr>");
            out.println("<th>Id</th>");
            out.println("<th>Nombre</th>");
            out.println("<th>Ciudad</th>");
            out.println("<th>Departamento</th>");
            out.println("<th>Contacto</th>");
            out.println("<th>Estado</th>");
            out.println("</tr>");
            
            Sucursal fila;
           
            
            for(int i = 0; i < listaSucursales.size(); i++){
                
                fila = listaSucursales.get(i);
                
                 //construccion de la fila usando el objeto fila
                 out.println("<tr>");
                 out.println("<td>" + fila.getId_sucursal()+ "</td>");
                 out.println("<td>" + fila.getNombre()+ "</td>");
                 out.println("<td>" + fila.getCiudad()+ "</td>");
                 out.println("<td>" + fila.getDepartamento()+ "</td>");
                 out.println("<td>" + fila.getContacto()+ "</td>");
                
                 //para imprimir el estado en la tabla   
                 String estadoString = fila.isEstado()?"activo":"inactivo";
                 
                 out.println("<td>" + estadoString+ "</td>");
                 
                 out.println("</tr>");
                
            }
            
            out.println("</table>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
