/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.control;

import java.util.ArrayList;



import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author Esperanza
 */
public class Motor {

    ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();
   
    public Motor() {
        sucursales.add(new Sucursal(1, "Cinepolis Metrocentro", "Santa Ana", "Santa Ana", "Juan Perez", true));
        sucursales.add(new Sucursal(2, "Cinepolis La Gran Via", "Antiguo Cuscatlan", "San Salvador", "Luis Lopez", true));
        sucursales.add(new Sucursal(3, "Cinepolis Metrocento SM", "San Miguel", "San Miguel", "Will Salgado", false));
        sucursales.add(new Sucursal(4, "Cinemark Metrocentro", "Soyapango", "San Salvador", "Encargado 1", false));
        sucursales.add(new Sucursal(5, "Cinepolis Metrocentro Sonso", "Sonsonate", "Sonsonate", "Encargado 2", true));
        sucursales.add(new Sucursal(6, "Cine La Union", "Puerto de la union", "La Union", "Encargado 3", true));
        sucursales.add(new Sucursal(7, "Cine La Union 2", "Ciudad de la union", "La Union", "Encargado 3", false));
    }
  
public ArrayList<Sucursal> mostrarSucursales() {return sucursales;}

    public boolean modificarSucursal(int id_sucursal, String nombre, String ciudad, String departamento, String contacto, boolean estado) {

        //la sucursal con la id n esta en la posicion n - 1        
        Sucursal editada = this.sucursales.get(id_sucursal - 1);
        
        //si esta activa, no se puede modificar
        if(editada.isEstado()){
            
            return false;
            
        }
        
        //si es string vacia, no edita
        if(!"".equals(nombre))editada.setNombre(nombre);
        if(!"".equals(ciudad))editada.setCiudad(ciudad);
        if(!"".equals(departamento))editada.setDepartamento(departamento);
        if(!"".equals(contacto))editada.setContacto(contacto);
        editada.setEstado(estado);
        
        //inserta la sucursal editada
        this.sucursales.set(id_sucursal-1, editada);
        
        //si tiene exito, retorna true
        return true;
        
    }

    public ArrayList<Sucursal> buscarActivos(String departamento) {
        
        //crear arreglo de activos
        ArrayList<Sucursal> activos = new ArrayList<>();
        
        //item a revisar
        Sucursal item = null;
        
        //para todo elemento en sucursales
        for(int i = 0; i < this.sucursales.size(); i++){
            
            //obtener item
            item = this.sucursales.get(i);
            
            //revisar condiciones
            if(!item.getDepartamento().equals(departamento)||item.getContacto().equals("Will Salgado")||!item.isEstado()){
                
                //si no se debe mostrar, se lo salta
                continue;
                
            }
            
            //si es valido, se agregan a la lista a devolver
            activos.add(item);
            
        }
        
        //devolver lista
        return activos;
        
    }


 
}
